﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Institutions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration.InstitutionConfiguration
{
    public class StudentConfig : IBaseEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            //Fields
            builder.HasKey(i => new { i.Id });
            builder.Property(i => i.FirstName).HasMaxLength(300).IsRequired();
            builder.Property(i => i.LastName).HasMaxLength(300).IsRequired();

            //Navigations
            builder.HasOne(i => i.Registerer).WithMany(p => p.RegistereStudents)
                   .HasForeignKey(i => i.RegistererID).OnDelete(DeleteBehavior.Cascade);

            //Table
            builder.ToTable(nameof(Student), SchemaConfig.Institution);
        }
    }
}