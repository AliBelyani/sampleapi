﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Institutions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration.InstitutionConfiguration
{
    public class StudentRollCallConfig : IBaseEntityTypeConfiguration<StudentRollCall>
    {
        public void Configure(EntityTypeBuilder<StudentRollCall> builder)
        {
            //Fields
            builder.HasKey(i => new { i.Id });

            //Navigations
            builder.HasOne(i => i.Teacher).WithMany(p => p.RegistereStudentRollCalls)
                   .HasForeignKey(i => i.TeacherId).OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(i => i.Student).WithMany(p => p.StudentRollCalls)
                   .HasForeignKey(i => i.StudentId).OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(i => i.Course).WithMany(p => p.StudentRollCalls)
                   .HasForeignKey(i => i.CourseId);

            //Table
            builder.ToTable(nameof(StudentRollCall), SchemaConfig.Institution);
        }
    }
}
