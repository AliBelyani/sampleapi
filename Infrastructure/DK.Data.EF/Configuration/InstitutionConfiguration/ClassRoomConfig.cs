﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Institutions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration.InstitutionConfiguration
{
    public class ClassRoomConfig : IBaseEntityTypeConfiguration<ClassRoom>
    {
        public void Configure(EntityTypeBuilder<ClassRoom> builder)
        {
            //Fields
            builder.HasKey(i => new { i.Id });
            builder.Property(i => i.Title).HasMaxLength(300).IsRequired();

            //Navigations
            builder.HasOne(i => i.Manager).WithMany(p => p.RegistereClassRooms)
                   .HasForeignKey(i => i.RegistererID).OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(i => i.Teacher).WithMany(p => p.TeacherClassRooms)
                   .HasForeignKey(i => i.TeacherId).OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(i => i.Course).WithMany(p => p.ClassRooms)
                   .HasForeignKey(i => i.CourseId);

            //Table
            builder.ToTable(nameof(ClassRoom), SchemaConfig.Institution);
        }
    }
}
