﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Institutions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration.InstitutionConfiguration
{
    public class StudentScoreConfig : IBaseEntityTypeConfiguration<StudentScore>
    {
        public void Configure(EntityTypeBuilder<StudentScore> builder)
        {
            //Fields
            builder.HasKey(i => new { i.Id });

            //Navigations
            builder.HasOne(i => i.Teacher).WithMany(p => p.RegistereStudentScores)
                   .HasForeignKey(i => i.TeacherId).OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(i => i.Student).WithMany(p => p.StudentScores)
                   .HasForeignKey(i => i.StudentId).OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(i => i.Course).WithMany(p => p.StudentScores)
                   .HasForeignKey(i => i.CourseId);

            //Table
            builder.ToTable(nameof(StudentScore), SchemaConfig.Institution);
        }
    }
}
