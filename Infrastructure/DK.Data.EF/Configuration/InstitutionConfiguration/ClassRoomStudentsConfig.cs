﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Institutions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration.InstitutionConfiguration
{
    public class ClassRoomStudentsConfig : IBaseEntityTypeConfiguration<ClassRoomStudents>
    {
        public void Configure(EntityTypeBuilder<ClassRoomStudents> builder)
        {
            //Fields
            builder.HasKey(i => new { i.ClassRoomId, i.StudentId });

            //Navigations
            builder.HasOne(i => i.ClassRoom).WithMany(p => p.ClassRoomStudents)
                   .HasForeignKey(i => i.ClassRoomId);

            builder.HasOne(i => i.Student).WithMany(p => p.ClassRoomStudents)
                   .HasForeignKey(i => i.StudentId).OnDelete(DeleteBehavior.ClientSetNull);

            //Table
            builder.ToTable(nameof(ClassRoomStudents), SchemaConfig.Institution);
        }
    }
}
