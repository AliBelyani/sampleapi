﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Institutions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration.InstitutionConfiguration
{
    public class CourseConfig : IBaseEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            //Fields
            builder.HasKey(i => new { i.Id });
            builder.Property(i => i.Title).HasMaxLength(300).IsRequired();

            //Navigations
            builder.HasOne(i => i.Registerer).WithMany(p => p.RegistereCourses)
                   .HasForeignKey(i => i.RegistererID).OnDelete(DeleteBehavior.Cascade);

            //Table
            builder.ToTable(nameof(Course), SchemaConfig.Institution);
        }
    }
}
