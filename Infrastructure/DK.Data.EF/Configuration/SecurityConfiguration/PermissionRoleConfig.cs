﻿using DK.Domain.Entity.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration.SecurityConfiguration
{
    public class PermissionRoleConfig : IBaseEntityTypeConfiguration<PermissionRole>
    {
        public void Configure(EntityTypeBuilder<PermissionRole> builder)
        {
            //Fields
            builder.HasKey(k => new { k.PermissionID, k.RoleID });

            //Relation
            builder.HasOne(c => c.Permission).WithMany(c => c.PermissionRoles)
                   .HasForeignKey(f => f.PermissionID);

            builder.HasOne(c => c.Role).WithMany(c => c.PermissionRoles)
                   .HasForeignKey(f => f.RoleID);

            //Table
            builder.ToTable("PermissionRoles", "Security");

        }
    }
}
