﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration.SecurityConfiguration
{
    public class PermissionGroupConfig : IBaseEntityTypeConfiguration<PermissionGroup>
    {
        public void Configure(EntityTypeBuilder<PermissionGroup> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);
            builder.Property(p => p.Name).HasMaxLength(100);

            //Relation
            builder.HasMany(i => i.PermissionGroups).WithOne(p => p.PermmissionGroup)
                   .HasForeignKey(i => i.ParentID);

            //Table
            builder.ToTable(nameof(PermissionGroup), SchemaConfig.Security);
        }
    }
}
