﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration
{
    public class UserRoleConfig : IBaseEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            //Fields
            builder.HasKey(i => new { i.UserId, i.RoleId });

            //Relations
            builder.HasOne(i => i.Role).WithMany(p => p.UserRoles)
                   .HasForeignKey(i => i.RoleId);

            builder.HasOne(i => i.User).WithMany(p => p.UserRoles)
                   .HasForeignKey(i => i.UserId);

            //Table
            builder.ToTable(nameof(UserRole), SchemaConfig.Security);
        }
    }
}