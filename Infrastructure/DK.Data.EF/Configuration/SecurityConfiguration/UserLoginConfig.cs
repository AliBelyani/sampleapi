﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration.SecurityConfiguration
{
    public class UserLoginConfig : IBaseEntityTypeConfiguration<UserLogin>
    {
        public void Configure(EntityTypeBuilder<UserLogin> builder)
        {
            //Fields
            builder.HasKey(i => new { i.LoginProvider, i.ProviderKey, i.UserId });

            //Table
            builder.ToTable(nameof(UserLogin), SchemaConfig.Security);
        }
    }
}