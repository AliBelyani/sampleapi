﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration
{
    public class RoleClaimConfig : IBaseEntityTypeConfiguration<RoleClaim>
    {
        public void Configure(EntityTypeBuilder<RoleClaim> builder)
        {
            builder.HasKey(i => i.Id);

            //Table
            builder.ToTable(nameof(RoleClaim), SchemaConfig.Security);
        }
    }
}