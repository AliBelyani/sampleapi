﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration.SecurityConfiguration
{
    public class UserConfig : IBaseEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            //Fields
            builder.HasKey(i => new { i.Id });
            builder.Property(i => i.FirstName).HasMaxLength(200);
            builder.Property(i => i.LastName).HasMaxLength(200);

            //Table
            builder.ToTable(nameof(User), SchemaConfig.Security);
        }
    }
}