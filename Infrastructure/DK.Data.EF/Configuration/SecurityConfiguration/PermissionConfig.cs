﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DK.Data.EF.Configuration.SecurityConfiguration
{
    public class PermissionConfig : IBaseEntityTypeConfiguration<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> builder)
        {
            //Fields
            builder.HasKey(k => k.Id);

            //Relation
            builder.HasOne(r => r.PermissionGroup).WithMany(r => r.Permissions)
                   .HasForeignKey(r => r.PermissionGroupID);

            //Table
            builder.ToTable(nameof(Permission), SchemaConfig.Security);
        }
    }
}