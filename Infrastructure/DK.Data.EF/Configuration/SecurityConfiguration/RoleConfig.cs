﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration
{
    public class RoleConfig : IBaseEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            //Fields
            builder.HasKey(i => i.Id);

            //navigation
            builder.HasMany<UserRole>().WithOne(ur => ur.Role).HasForeignKey(ur => ur.RoleId);
            builder.HasMany<RoleClaim>().WithOne().HasForeignKey(rc => rc.RoleId);

            //Table
            builder.ToTable(nameof(Role), SchemaConfig.Security);
        }
    }
}