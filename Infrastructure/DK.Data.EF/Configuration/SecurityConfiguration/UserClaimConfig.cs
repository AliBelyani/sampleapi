﻿using DK.Data.EF.Configuration.Base;
using DK.Domain.Entity.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Data.EF.Configuration
{
    public class UserClaimConfig : IBaseEntityTypeConfiguration<UserClaim>
    {
        public void Configure(EntityTypeBuilder<UserClaim> builder)
        {
            //Fields
            builder.HasKey(i => i.Id);

            //Table
            builder.ToTable(nameof(UserClaim), SchemaConfig.Security);
        }
    }
}