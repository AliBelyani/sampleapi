﻿namespace DK.Data.EF.Configuration.Base
{
    public static class SchemaConfig
    {
        public static string Security { get { return "Security"; } }
        public static string Institution { get { return "Institution"; } }
    }
}
