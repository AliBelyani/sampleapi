﻿using AutoMapper;
using DK.Data.EF.Context;
using DK.Domain.Entity.Institutions;
using DK.Domain.Entity.Security;
using DK.Service.Interface.Repository;
using System.Threading.Tasks;

namespace DK.Data.EF.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDBContext _context;

        #region Ctor
        IMapper _mapper;
        public UnitOfWork(ApplicationDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        #endregion

        private IGenericRepository<User> _UserRepository;
        private IGenericRepository<Role> _RoleRepository;
        private IGenericRepository<Permission> _PermissionRepository;
        private IGenericRepository<PermissionGroup> _PermissionGroupRepository;
        private IGenericRepository<ClassRoom> _ClassRoomRepository;
        private IGenericRepository<Course> _CourseRepository;
        private IGenericRepository<Student> _StudentRepository;
        private IGenericRepository<StudentRollCall> _StudentRollCallRepository;
        private IGenericRepository<StudentScore> _StudentScoreRepository;

        public IGenericRepository<User> UserRepository
        {
            get
            {
                return _UserRepository ?? (_UserRepository = new GenericRepository<User>(_context, _mapper));
            }
        }

        public IGenericRepository<Role> RoleRepository
        {
            get
            {
                return _RoleRepository ?? (_RoleRepository = new GenericRepository<Role>(_context, _mapper));
            }
        }

        public IGenericRepository<Permission> PermissionRepository
        {
            get
            {
                return _PermissionRepository ?? (_PermissionRepository = new GenericRepository<Permission>(_context, _mapper));
            }
        }

        public IGenericRepository<PermissionGroup> PermissionGroupRepository
        {
            get
            {
                return _PermissionGroupRepository ?? (_PermissionGroupRepository = new GenericRepository<PermissionGroup>(_context, _mapper));
            }
        }

        public IGenericRepository<ClassRoom> ClassRoomRepository
        {
            get
            {
                return _ClassRoomRepository ?? (_ClassRoomRepository = new GenericRepository<ClassRoom>(_context, _mapper));
            }
        }

        public IGenericRepository<Course> CourseRepository
        {
            get
            {
                return _CourseRepository ?? (_CourseRepository = new GenericRepository<Course>(_context, _mapper));
            }
        }

        public IGenericRepository<Student> StudentRepository
        {
            get
            {
                return _StudentRepository ?? (_StudentRepository = new GenericRepository<Student>(_context, _mapper));
            }
        }

        public IGenericRepository<StudentRollCall> StudentRollCallRepository
        {
            get
            {
                return _StudentRollCallRepository ?? (_StudentRollCallRepository = new GenericRepository<StudentRollCall>(_context, _mapper));
            }
        }

        public IGenericRepository<StudentScore> StudentScoreRepository
        {
            get
            {
                return _StudentScoreRepository ?? (_StudentScoreRepository = new GenericRepository<StudentScore>(_context, _mapper));
            }
        }

        #region Base
        public void Dispose()
        {
            _context.Dispose();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
        #endregion
    }
}
