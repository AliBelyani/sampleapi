﻿using AutoMapper;
using DK.Domain.DTO.Security;
using DK.Domain.Entity.Security;
using DK.Service.Interface.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DK.Service.Service.Security
{
    public interface IPermissionService
    {
        Task<bool> UpdateList(List<PermissionGroupDto> PermissionGroups);
    }

    public class PermissionService : BaseService, IPermissionService
    {
        public PermissionService(IUnitOfWork uow, IMapper IMapper) : base(uow, IMapper)
        {
        }

        public async Task<bool> UpdateList(List<PermissionGroupDto> PermissionGroups)
        {
            var dBPermissions = await uow.PermissionRepository.GetAsync();
            var dBPermissionGroups = await uow.PermissionGroupRepository.GetAsync();

            foreach (var Group in PermissionGroups)
            {
                var permissionGroup = dBPermissionGroups.FirstOrDefault(a => a.ControllerName == Group.ControllerName);

                if (permissionGroup == null)
                {
                    var newGroupDB = new PermissionGroup { Name = Group.Name, ControllerName = Group.ControllerName };
                    uow.PermissionGroupRepository.Insert(newGroupDB);

                    foreach (var xItem in Group.Permissions)
                    {
                        var permission = dBPermissions.FirstOrDefault(a => a.Action == xItem.Action && a.Controller == Group.ControllerName);

                        if (permission == null)
                            uow.PermissionRepository.Insert(new Permission
                            {
                                PermissionGroupID = newGroupDB.Id,
                                Controller = Group.ControllerName,
                                Action = xItem.Action,
                                Name = xItem.Name,
                                ActionType = xItem.ActionType,
                            });
                        else
                        {
                            if (permission.Name != xItem.Name)
                                permission.Name = xItem.Name;

                            if (permission.ActionType != xItem.ActionType)
                                permission.ActionType = xItem.ActionType;

                            if (permission.PermissionGroupID != newGroupDB.Id)
                                permission.PermissionGroupID = newGroupDB.Id;

                            uow.PermissionRepository.Update(permission);
                        }
                    }
                }
                else
                {
                    if (permissionGroup.Name != Group.Name)
                    {
                        permissionGroup.Name = Group.Name;
                        uow.PermissionGroupRepository.Update(permissionGroup);
                    }

                    foreach (var xItem in Group.Permissions)
                    {
                        var permission = dBPermissions.FirstOrDefault(a => a.Action == xItem.Action && a.Controller == Group.ControllerName);

                        if (permission == null)
                            uow.PermissionRepository.Insert(new Permission
                            {
                                PermissionGroupID = permissionGroup.Id,
                                Controller = Group.ControllerName,
                                Action = xItem.Action,
                                Name = xItem.Name,
                                ActionType = xItem.ActionType
                            });
                        else
                        {
                            if (permission.Name != xItem.Name)
                                permission.Name = xItem.Name;

                            if (permission.ActionType != xItem.ActionType)
                                permission.ActionType = xItem.ActionType;

                            if (permission.PermissionGroupID != permissionGroup.Id)
                                permission.PermissionGroupID = permissionGroup.Id;

                            uow.PermissionRepository.Update(permission);
                        }
                    }
                }
                await uow.SaveChangesAsync();
            }
            await uow.SaveChangesAsync();
            return true;
        }
    }
}
