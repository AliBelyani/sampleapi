﻿using System.Threading.Tasks;
using AutoMapper;
using DK.Domain.Entity.Security;
using DK.Service.Interface.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DK.Service.Service.Security
{
    public interface IUserService
    {
        int GetCurrentUserID();
        bool IsLogin();
        bool CheckPermission(string ControllerName, string ActionName, int? UserID = null);
        Task<bool> AddUsersWithRole();
    }

    public class UserService : BaseService, IUserService
    {
        #region == Ctor ==
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(IUnitOfWork uow, IMapper IMapper, UserManager<User> userManager,
                           IHttpContextAccessor httpContextAccessor) : base(uow, IMapper)
        {
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }
        #endregion

        public bool CheckPermission(string controllerName, string actionName, int? userID = null)
        {
            if (userID == null)
                userID = GetCurrentUserID();

            return uow.UserRepository.Any(w => w.Id == userID.Value && w.UserRoles.SelectMany(x => x.Role.PermissionRoles)
                                     .Any(d => d.Permission.Controller == controllerName && d.Permission.Action == actionName));
        }

        public int GetCurrentUserID()
        {
            if (IsLogin())
            {
                if (int.TryParse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier), out int xResult))
                    return xResult;
                else
                    return 0;
            }
            else
                return 0;
        }

        public bool IsLogin()
        {
            return _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated;
        }

        public async Task<bool> AddUsersWithRole()
        {
            var managerRole = new Role { Name = "ManagerRole" };
            var moderatorRole = new Role { Name = "ModeratorRole" };
            var teacherRole = new Role { Name = "TeacherRole" };

            uow.RoleRepository.Insert(managerRole);
            uow.RoleRepository.Insert(moderatorRole);
            uow.RoleRepository.Insert(teacherRole);
            await uow.SaveChangesAsync();

            var manager = new User()
            {
                FirstName = "Manager",
                LastName = "Manager",
                Email = "Manager@gmail.com",
                PhoneNumber = "09121121212",
                UserName = "Manager",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true
            };
            var moderator = new User()
            {
                FirstName = "Moderator",
                LastName = "Moderator",
                Email = "Moderator@gmail.com",
                PhoneNumber = "09121121212",
                UserName = "Moderator",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true
            };
            var teacher = new User()
            {
                FirstName = "Teacher",
                LastName = "Teacher",
                Email = "Teacher@gmail.com",
                PhoneNumber = "09121121212",
                UserName = "Teacher",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true
            };

            manager.UserRoles.Add(new UserRole { UserId = manager.Id, RoleId = managerRole.Id });
            moderator.UserRoles.Add(new UserRole { UserId = moderator.Id, RoleId = moderatorRole.Id });
            teacher.UserRoles.Add(new UserRole { UserId = teacher.Id, RoleId = teacherRole.Id });

            await _userManager.CreateAsync(manager, "888888");
            await _userManager.CreateAsync(moderator, "888888");
            await _userManager.CreateAsync(teacher, "888888");

            return true;
        }
    }
}