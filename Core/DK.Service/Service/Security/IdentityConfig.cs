﻿using DK.Domain.Entity.Security;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DK.Service.Service.Security
{
    public class ApplicationSignInManager : SignInManager<User>
    {
        #region == Ctor ==
        public ApplicationSignInManager(UserManager<User> userManager, IHttpContextAccessor contextAccessor,
          IUserClaimsPrincipalFactory<User> claimsFactory, IOptions<IdentityOptions> optionsAccessor,
          ILogger<SignInManager<User>> logger, IAuthenticationSchemeProvider schemes)
              : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes)
        { }
        #endregion
    }
}
