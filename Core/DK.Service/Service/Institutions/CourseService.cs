﻿using AutoMapper;
using DK.Domain.DTO.Institutions.Courses;
using DK.Domain.Entity.Institutions;
using DK.Service.Interface.Repository;
using DK.Service.Service.Security;
using System;
using System.Threading.Tasks;

namespace DK.Service.Service.Institutions
{
    public interface ICourseService
    {
        Task<bool> Add(AddCourseDto addCourse);
    }

    public class CourseService : BaseService, ICourseService
    {
        private readonly IUserService _userService;

        public CourseService(IUnitOfWork uow, IMapper IMapper, IUserService userService) : base(uow, IMapper)
        {
            this._userService = userService;
        }

        public async Task<bool> Add(AddCourseDto addCourse)
        {
            var course = _IMapper.Map<Course>(addCourse);
            course.RegisterDate = DateTime.Now;
            course.RegistererID = _userService.GetCurrentUserID();
            course.IsDeleted = false;

            uow.CourseRepository.Insert(course);
            await uow.SaveChangesAsync();
            return true;
        }
    }
}