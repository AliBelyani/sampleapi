﻿using AutoMapper;
using DK.Domain.DTO.Institutions.Students;
using DK.Domain.Entity.Institutions;
using DK.Service.Interface.Repository;
using DK.Service.Service.Security;
using System;
using System.Threading.Tasks;

namespace DK.Service.Service.Institutions
{
    public interface IStudentService
    {
        Task<bool> Add(AddStudentDto addStudent);
        Task<bool> AddStudentRollCall(AddStudentRollCallDto studentRollCall);
        Task<bool> AddStudentScoreDto(AddStudentScoreDto studentScoreDto);
    }

    public class StudentService : BaseService, IStudentService
    {
        private readonly IUserService _userService;

        public StudentService(IUnitOfWork uow, IMapper IMapper, IUserService userService) : base(uow, IMapper)
        {
            this._userService = userService;
        }

        public async Task<bool> Add(AddStudentDto addStudent)
        {
            var student = _IMapper.Map<Student>(addStudent);
            student.RegisterDate = DateTime.Now;
            student.RegistererID = _userService.GetCurrentUserID();
            student.IsDeleted = false;

            uow.StudentRepository.Insert(student);
            await uow.SaveChangesAsync();
            return true;
        }

        public async Task<bool> AddStudentRollCall(AddStudentRollCallDto studentRollCall)
        {
            var currentUserId = _userService.GetCurrentUserID();
            foreach (var student in studentRollCall.StudentRollCalls)
            {
                uow.StudentRollCallRepository.Insert(new StudentRollCall
                {
                    CourseId = studentRollCall.CourseId,
                    IsPresent = student.IsPresent,
                    RollCallDate = DateTime.Now,
                    StudentId = student.StudentId,
                    TeacherId = currentUserId
                });
            }
            await uow.SaveChangesAsync();
            return true;
        }

        public async Task<bool> AddStudentScoreDto(AddStudentScoreDto studentScoreDto)
        {
            var currentUserId = _userService.GetCurrentUserID();
            foreach (var student in studentScoreDto.StudentScores)
            {
                uow.StudentScoreRepository.Insert(new StudentScore
                {
                    CourseId = studentScoreDto.CourseId,
                    Score = student.Score,
                    RegisterDate = DateTime.Now,
                    StudentId = student.StudentId,
                    TeacherId = currentUserId
                });
            }
            await uow.SaveChangesAsync();
            return true;
        }
    }
}