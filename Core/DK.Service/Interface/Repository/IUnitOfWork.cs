﻿using DK.Domain.Entity.Security;
using System;
using System.Threading.Tasks;
using DK.Domain.Entity.Institutions;

namespace DK.Service.Interface.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<User> UserRepository { get; }
        IGenericRepository<Role> RoleRepository { get; }
        IGenericRepository<Permission> PermissionRepository { get; }
        IGenericRepository<PermissionGroup> PermissionGroupRepository { get; }
        IGenericRepository<ClassRoom> ClassRoomRepository { get; }
        IGenericRepository<Course> CourseRepository { get; }
        IGenericRepository<Student> StudentRepository { get; }
        IGenericRepository<StudentRollCall> StudentRollCallRepository { get; }
        IGenericRepository<StudentScore> StudentScoreRepository { get; }

        void SaveChanges();
        Task SaveChangesAsync();
    }
}
