﻿using DK.Domain.Entity.Security;
using DK.Domain.Enumeration;
using System.Collections.Generic;

namespace DK.Domain.DTO.Security
{
    [DtoFor(typeof(PermissionGroup))]
    public class PermissionGroupDto
    {
        public long? ParentID { get; set; }

        public string Name { get; set; }

        public string ControllerName { get; set; }

        public List<PermissionDto> Permissions { get; set; }
    }

    public class PermissionDto
    {
        public string Name { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public ActionType ActionType { get; set; }

        public string Comment { get; set; }

        public long? PermissionGroupID { get; set; }

    }
}
