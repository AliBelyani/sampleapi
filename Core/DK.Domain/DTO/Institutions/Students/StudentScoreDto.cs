﻿namespace DK.Domain.DTO.Institutions.Students
{
    public class StudentScoreDto
    {
        public int StudentId { get; set; }

        public decimal Score { get; set; }
    }
}
