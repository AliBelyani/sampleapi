﻿using System;
using System.Collections.Generic;

namespace DK.Domain.DTO.Institutions.Students
{
    public class AddStudentScoreDto
    {
        public int CourseId { get; set; }

        public int TeacherId { get; set; }

        public DateTime RegisterDate { get; set; }

        public List<StudentScoreDto> StudentScores { get; set; }
    }
}
