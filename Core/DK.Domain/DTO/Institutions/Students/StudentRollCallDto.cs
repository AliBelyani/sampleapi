﻿namespace DK.Domain.DTO.Institutions.Students
{
    public class StudentRollCallDto
    {
        public int StudentId { get; set; }

        public bool IsPresent { get; set; }
    }
}
