﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DK.Domain.DTO.Institutions.Students
{
    public class AddStudentRollCallDto
    {
        public int TeacherId { get; set; }

        public int CourseId { get; set; }

        public DateTime RollCallDate { get; set; }

        public List<StudentRollCallDto> StudentRollCalls { get; set; }
    }
}
