﻿using DK.Domain.Entity.Institutions;

namespace DK.Domain.DTO.Institutions.Students
{
    [DtoFor(typeof(Student))]
    public class AddStudentDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
