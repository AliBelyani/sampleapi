﻿using DK.Domain.Entity.Institutions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DK.Domain.DTO.Institutions.Courses
{
    [DtoFor(typeof(Course))]
   public class AddCourseDto
    {
        public string Title { get; set; }
        public byte Unit { get; set; }
    }
}
