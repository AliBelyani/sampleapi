﻿using DK.Domain.Entity.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace DK.Domain.Entity.Base
{
    public abstract class BaseEntity<T> : IEntity<T>, IEntity
    { 
        public T Id { get; set; }
    }

    public interface IEntity
    { }
    public interface IEntity<T>
    {
        T Id { get; set; }
    }

    public interface ISoftDelete
    {
        bool IsDeleted { get; set; }
    }

    public interface IRegisterDate<TRegistererID>
    {
        DateTime RegisterDate { get; set; }
        TRegistererID RegistererID { get; set; }
        
    }
    public interface IModifiedDate<TModifierID>
        where TModifierID: struct
    {
        DateTime? ModifyDate { get; set; }
        TModifierID? ModifierID { get; set; }
    }
}
