﻿using DK.Domain.Entity.Base;
using DK.Domain.Entity.Security;
using System;

namespace DK.Domain.Entity.Institutions
{
    /// <summary>
    /// نمره دانش آموز
    /// </summary>
    public class StudentScore : BaseEntity<int>
    {
        #region Field
        /// <summary>
        /// کلید درس
        /// </summary>
        public int CourseId { get; set; }

        /// <summary>
        /// کلید دانش آموز
        /// </summary>
        public int StudentId { get; set; }

        /// <summary>
        /// نمره
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// کلید معلم ثبت کتتده
        /// </summary>
        public int TeacherId { get; set; }

        /// <summary>
        /// تاریخ ثبت
        /// </summary>
        public DateTime RegisterDate { get; set; }
        #endregion

        #region Navigation
        /// <summary>
        /// معلم ثبت کننده
        /// </summary>
        public User Teacher { get; set; }

        /// <summary>
        /// درس
        /// </summary>
        public Course Course { get; set; }

        /// <summary>
        /// دانش آموز
        /// </summary>
        public Student Student { get; set; }
        #endregion
    }
}
