﻿using DK.Domain.Entity.Base;

namespace DK.Domain.Entity.Institutions
{
    /// <summary>
    /// دانش آموزان کلاس
    /// </summary>
    public class ClassRoomStudents : IEntity
    {
        #region Field
        /// <summary>
        /// کلید کلاس
        /// </summary>
        public int ClassRoomId { get; set; }

        /// <summary>
        /// کلید دانش آموز
        /// </summary>
        public int StudentId { get; set; }
        #endregion

        #region Navigation
        /// <summary>
        /// کلاس
        /// </summary>
        public ClassRoom ClassRoom { get; set; }

        /// <summary>
        /// دانش آموز
        /// </summary>
        public Student Student { get; set; }
        #endregion
    }
}
