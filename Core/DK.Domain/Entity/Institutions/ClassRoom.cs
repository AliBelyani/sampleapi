﻿using DK.Domain.Entity.Base;
using DK.Domain.Entity.Security;
using System;
using System.Collections.Generic;

namespace DK.Domain.Entity.Institutions
{
    /// <summary>
    /// کلاس
    /// </summary>
    public class ClassRoom : BaseEntity<int>, IRegisterDate<int>, ISoftDelete
    {
        #region Field
        /// <summary>
        /// کلید معلم
        /// </summary>
        public int TeacherId { get; set; }

        /// <summary>
        /// کلید درس
        /// با این فرض که هر کلاس فقط می تواند در آن یک درس تدریس شود
        /// </summary>
        public int CourseId { get; set; }

        /// <summary>
        /// عنوان یا نام کلاس 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// ظرفیت
        /// </summary>
        public byte Capacity { get; set; }

        /// <summary>
        /// آیا رکورد حذف شده است؟
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// تاریخ ثبت
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// ثبت کننده
        /// </summary>
        public int RegistererID { get; set; }
        #endregion

        #region Navigation
        /// <summary>
        /// ناظم ثبت کننده
        /// </summary>
        public User Manager { get; set; }

        /// <summary>
        /// معلم
        /// </summary>
        public User Teacher { get; set; }

        /// <summary>
        /// لیست دانش آموزان کلاس
        /// </summary>
        public ICollection<ClassRoomStudents> ClassRoomStudents { get; set; }

        /// <summary>
        /// درس
        /// </summary>
        public Course Course { get; set; }
        #endregion
    }
}
