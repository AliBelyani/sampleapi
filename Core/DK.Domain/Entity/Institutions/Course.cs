﻿using DK.Domain.Entity.Base;
using DK.Domain.Entity.Security;
using System;
using System.Collections.Generic;

namespace DK.Domain.Entity.Institutions
{
    /// <summary>
    /// درس
    /// </summary>
    public class Course : BaseEntity<int>, IRegisterDate<int>, ISoftDelete
    {
        #region Field
        /// <summary>
        /// عنوان
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// واحد درسی
        /// </summary>
        public byte Unit { get; set; }

        /// <summary>
        /// آیا رکورد حذف شده است؟
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// تاریخ ثبت
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// ثبت کننده
        /// </summary>
        public int RegistererID { get; set; }
        #endregion

        #region Navigation
        /// <summary>
        /// کاربر ثبت کننده
        /// </summary>
        public User Registerer { get; set; }

        /// <summary>
        /// لیست کلاس ها
        /// </summary>
        public ICollection<ClassRoom> ClassRooms { get; set; }

        /// <summary>
        /// لیست نمرات دانش آموز
        /// </summary>
        public ICollection<StudentScore> StudentScores { get; set; }

        /// <summary>
        /// لیست حضور و غیاب دانش آموز
        /// </summary>
        public ICollection<StudentRollCall> StudentRollCalls { get; set; }
        #endregion
    }
}
