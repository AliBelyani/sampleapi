﻿using DK.Domain.Entity.Base;
using DK.Domain.Entity.Security;
using System;

namespace DK.Domain.Entity.Institutions
{
    /// <summary>
    /// حضور و غیاب دانش آموزان
    /// </summary>
    public class StudentRollCall : BaseEntity<int>
    {
        #region Field
        /// <summary>
        /// کلید معلم ثبت کتتده
        /// </summary>
        public int TeacherId { get; set; }

        /// <summary>
        /// کلید درس
        /// </summary>
        public int CourseId { get; set; }

        /// <summary>
        /// کلید دانش آموز
        /// </summary>
        public int StudentId { get; set; }

        /// <summary>
        /// تاریخ حضور و غیاب
        /// </summary>
        public DateTime RollCallDate { get; set; }

        /// <summary>
        /// آیا حضور دارد؟
        /// </summary>
        public bool IsPresent { get; set; }
        #endregion

        #region Navigation
        /// <summary>
        /// معلم ثبت کننده
        /// </summary>
        public User Teacher { get; set; }

        /// <summary>
        /// درس
        /// </summary>
        public Course Course { get; set; }

        /// <summary>
        /// دانش آموز
        /// </summary>
        public Student Student { get; set; }
        #endregion
    }
}
