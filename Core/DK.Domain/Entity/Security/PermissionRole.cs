﻿using DK.Domain.Entity.Base;

namespace DK.Domain.Entity.Security
{
    /// <summary>
    /// دسترسی های نقش
    /// </summary>
    public class PermissionRole : IEntity
    {
        #region == Field ==
        /// <summary>
        /// کلید دسترسی
        /// </summary>
        public int PermissionID { get; set; }

        /// <summary>
        /// کلید نقش
        /// </summary>
        public int RoleID { get; set; }
        #endregion

        #region == Navigation ==
        /// <summary>
        /// دسترسی
        /// </summary>
        public Permission Permission { get; set; }

        /// <summary>
        /// نقش
        /// </summary>
        public Role Role { get; set; }
        #endregion
    }
}
