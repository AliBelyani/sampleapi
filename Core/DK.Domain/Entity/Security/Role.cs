﻿using DK.Domain.Entity.Base;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace DK.Domain.Entity.Security
{
    public class Role : IdentityRole<int>, IEntity
    {
        public ICollection<UserRole> UserRoles { get; set; }

        public ICollection<PermissionRole> PermissionRoles { get; set; }
    }
}
