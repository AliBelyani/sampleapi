﻿using DK.Domain.Entity.Base;
using System.Collections.Generic;

namespace DK.Domain.Entity.Security
{
    /// <summary>
    /// گروه دسترسی
    /// </summary>
    public class PermissionGroup : BaseEntity<long>
    {
        #region == Field ==
        /// <summary>
        /// کلید پدر
        /// </summary>
        public long? ParentID { get; set; }

        /// <summary>
        /// نام
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// نام کنترلر
        /// </summary>
        public string ControllerName { get; set; }
        #endregion

        #region == Navigation ==
        /// <summary>
        /// گروه دسترسی
        /// </summary>
        public PermissionGroup PermmissionGroup { get; set; }

        /// <summary>
        /// لیست گروه دسترسی
        /// </summary>
        public ICollection<PermissionGroup> PermissionGroups { get; set; }

        /// <summary>
        /// لیست دسترسی ها
        /// </summary>
        public ICollection<Permission> Permissions { get; set; }
        #endregion
    }
}
