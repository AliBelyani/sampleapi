﻿using System.Collections.Generic;
using DK.Domain.Entity.Base;
using DK.Domain.Entity.Institutions;
using Microsoft.AspNetCore.Identity;

namespace DK.Domain.Entity.Security
{
    public class User : IdentityUser<int>, IEntity
    {
        public User()
        {
            UserRoles = new List<UserRole>();
        }

        #region == Field ==
        /// <summary>
        /// نام
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// نام خانوادگی
        /// </summary>
        public string LastName { get; set; }
        #endregion

        #region == Navigation ==
        public ICollection<UserRole> UserRoles { get; set; }
        public ICollection<StudentScore> RegistereStudentScores { get; set; }
        public ICollection<StudentRollCall> RegistereStudentRollCalls { get; set; }
        public ICollection<Course> RegistereCourses { get; set; }
        public ICollection<Student> RegistereStudents { get; set; }
        public ICollection<ClassRoom> RegistereClassRooms { get; set; }
        public ICollection<ClassRoom> TeacherClassRooms { get; set; }
        #endregion
    }
}
