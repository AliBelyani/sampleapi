﻿using DK.Domain.Entity.Base;
using DK.Domain.Enumeration;
using System.Collections.Generic;

namespace DK.Domain.Entity.Security
{
    /// <summary>
    /// دسترسی
    /// </summary>
    public class Permission : BaseEntity<int>
    {
        #region == Field ==
        /// <summary>
        /// نام دسترسی
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// کنترلر دسترس
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// اکشن دسترس
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// نوع اکشن
        /// </summary>
        public ActionType ActionType { get; set; }

        /// <summary>
        /// کلید گروه دسترسی
        /// </summary>
        public long PermissionGroupID { get; set; }
        #endregion

        #region == Navigation ==
        /// <summary>
        /// گروه دسترسی
        /// </summary>
        public PermissionGroup PermissionGroup { get; set; }

        /// <summary>
        /// دسترسی های نقش
        /// </summary>
        public ICollection<PermissionRole> PermissionRoles { get; set; }
        #endregion
    }
}
