﻿using Microsoft.AspNetCore.Identity;

namespace DK.Domain.Entity.Security
{
    public class UserRole: IdentityUserRole<int>
    {
        public User User { get; set; }
        public Role Role { get; set; }
    }
}
