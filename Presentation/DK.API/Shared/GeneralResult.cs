﻿using DK.Domain.DTO.Base;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace DK.API
{
    public sealed class GeneralResult : IConvertToActionResult
    {
        public GeneralResult(ActionResult result)
        {
            Result = result;
        }

        public ActionResult Result { get; }

        public static implicit operator GeneralResult(ActionResult result)
        {
            if (result is NOkObjectResult)
            {
                var xGeneralResult = new GeneralResponse
                {
                    xMessages = ((NOkObjectResult)result).xErrorMessages,
                    xResult = ((OkObjectResult)result)?.Value,
                    xStatus = ((NOkObjectResult)result).xErrorMessages == null || !((NOkObjectResult)result).xErrorMessages.Any()
                };

                return new GeneralResult(new OkObjectResult(xGeneralResult));
            }
            else if(result is OkObjectResult)
            {
                var xGeneralResult = new GeneralResponse
                {
                    xMessages = new List<string>(),
                    xResult = ((OkObjectResult)result)?.Value,
                    xStatus = true

                };

                return new GeneralResult(new OkObjectResult(xGeneralResult));
            }
            else if (result is BadRequestResult)
            {
                return new GeneralResult(result);
            }
            else if (result is BadRequestObjectResult)
            {
                return new GeneralResult(result);
            }
            else if (result is CreatedAtActionResult)
            {
                var xGeneralResult = new GeneralResponse
                {
                    xMessages = new List<string>(),
                    xResult = ((CreatedAtActionResult)result)?.Value,
                    xStatus = true

                };

                return new GeneralResult(new OkObjectResult(xGeneralResult));
            }        
            else if (result is NotFoundResult)
            {
                return new GeneralResult(result);
            }
            else
            {
                return new GeneralResult(result);
            }
        }

        IActionResult IConvertToActionResult.Convert()
        {
            return Result ?? new StatusCodeResult(500);
        }
    }
}