﻿using DK.Domain.Enumeration;
using Microsoft.AspNetCore.Authorization;
using System;

namespace DK.API.Shared
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class PermissionAttribute : AuthorizeAttribute
    {
        public string Name { get; }
        public ActionType ActionType { get; }

        public PermissionAttribute(string Name, ActionType ActionType) : base("Permission")
        {
            this.Name = Name;
            this.ActionType = ActionType;
        }
    }
}
