﻿using DK.API.Middlewares;
using DK.Domain.DTO.Others;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;

namespace DK.API.Ioc
{
    public static class ApiAuthorizationIoc
    {
        public static void AddApiAuthorization(this IServiceCollection services, IdentityServerSettings settings)
        {
            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = "http://localhost:5000";
                    options.RequireHttpsMetadata = false;
                    options.Audience = "BMSAPI";
                });

            services.AddScoped<IAuthorizationHandler, AttributeAuthorizationHandler>();

            services.AddAuthorization(option =>
                option.AddPolicy("Permission", builder =>
                    builder.AddRequirements(new PermissionAuthorizationRequirement())
                        .RequireAuthenticatedUser()
                )
            );
        }
    }
}
