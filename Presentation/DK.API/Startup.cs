﻿using DK.Data.EF.Context;
using DK.Data.EF.Repository;
using DK.Service.Interface.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DK.Domain.Mapper.Ioc;
using DK.Service.Ioc;
using DK.API.Ioc;
using DK.Domain.DTO.Others;
using Microsoft.AspNetCore.Http;
using DK.API.Middlewares.ExceptionHandler;
using DK.API.Shared;

namespace CleanArchitecture
{
    public class Startup
    {
        private readonly IdentityServerSettings identityServerSettings;
        private readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public IConfiguration Configuration { get; }
        private IHostingEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            identityServerSettings = configuration.GetSection(nameof(IdentityServerSettings)).Get<IdentityServerSettings>();
            Configuration = configuration;
            Environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IdentityServerSettings>(Configuration.GetSection(nameof(IdentityServerSettings)));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddMapperConfigurations();
            services.AddServices();

            services.AddDbContext<ApplicationDBContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("AppConnectionString"))
            );

            services.AddMvcCore().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddAuthorization()
                .AddJsonFormatters();

            services.AddCustomizedIdentityServer4(identityServerSettings, Environment);
            services.AddApiAuthorization(identityServerSettings);

            services.AddCors(o => o.AddPolicy(MyAllowSpecificOrigins, builder =>
           {
               builder.AllowAnyOrigin()
                      .AllowAnyMethod()
                      .AllowAnyHeader()
                      .AllowCredentials()
                      .WithExposedHeaders("Content-Disposition");
           }));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            app.UseCors(MyAllowSpecificOrigins);
            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.UseIdentityServer();
            app.UseHttpContext();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "api/{controller}/{action}/{id?}");
            });
        }
    }
}