﻿using System.ComponentModel;
using System.Threading.Tasks;
using DK.API.Shared;
using DK.Domain.DTO.Institutions.Courses;
using DK.Domain.Enumeration;
using DK.Service.Service.Institutions;
using Microsoft.AspNetCore.Mvc;

namespace DK.API.Controllers
{
    [Description("Course")]
    public class CourseController : ApiControllerBase
    {
        private readonly ICourseService _courseService;
        public CourseController(ICourseService courseService)
        {
            this._courseService = courseService;
        }

        [Permission("AddCourse", ActionType.Insert)]
        [HttpPost]
        public async Task<GeneralResult> Post(AddCourseDto addCourse)
        {
            return Ok(await _courseService.Add(addCourse));
        }
    }
}
