﻿using System.ComponentModel;
using System.Threading.Tasks;
using DK.API.Shared;
using DK.Domain.DTO.Institutions.Students;
using DK.Domain.Enumeration;
using DK.Service.Service.Institutions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DK.API.Controllers
{
    [Description("Student")]
    public class StudentController : ApiControllerBase
    {
        private readonly IStudentService _studentService;

        public StudentController(IStudentService studentService)
        {
            this._studentService = studentService;
        }

        public GeneralResult Get()
        {
            return Ok(1);
        }

        [HttpPost("AddStudent")]
        [Permission("AddStudent", ActionType.Insert)]
        public async Task<GeneralResult> Post(AddStudentDto addStudent)
        {
            return Ok(await _studentService.Add(addStudent));
        }

        [Permission("AddStudentRollCall", ActionType.Insert)]
        [HttpPost("AddStudentRollCall")]
        public async Task<GeneralResult> AddStudentRollCall(AddStudentRollCallDto addStudentRollCall)
        {
            return Ok(await _studentService.AddStudentRollCall(addStudentRollCall));
        }

        [Permission("AddStudentScore", ActionType.Insert)]
        [HttpPost("AddStudentScore")]
        public async Task<GeneralResult> AddStudentScore(AddStudentScoreDto addStudentScore)
        {
            return Ok(await _studentService.AddStudentScoreDto(addStudentScore));
        }
    }
}
