﻿using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using DK.API.Shared;
using DK.Domain.DTO.Security;
using DK.Service.Service.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace DK.API.Controllers.Security
{
    public class UserController : ApiControllerBase
    {
        #region == Ctor ==
        private readonly IUserService _UserService;
        private readonly IPermissionService _permissionService;
        private readonly IActionDescriptorCollectionProvider _actionDescriptorCollectionProvider;

        public UserController(IUserService UserService, IPermissionService permissionService, IActionDescriptorCollectionProvider actionDescriptorCollectionProvider)
        {
            _UserService = UserService;
            this._permissionService = permissionService;
            _actionDescriptorCollectionProvider = actionDescriptorCollectionProvider;
        }
        #endregion

        [HttpPost]
        public async Task<GeneralResult> Post()
        {
            return Ok(await _UserService.AddUsersWithRole());
        }

        [HttpGet]
        public async Task<GeneralResult> UpdatePermissions()
        {
            var TotalList = _actionDescriptorCollectionProvider.ActionDescriptors.Items.OfType<ControllerActionDescriptor>().Where(w => w.MethodInfo.CustomAttributes.Any(a => a.AttributeType == typeof(PermissionAttribute))).ToList();
            var PermissionGroups = TotalList.Select(z => z.ControllerName).Distinct().Select(ControllerName => new PermissionGroupDto
            {
                Name = TotalList.Where(w => w.ControllerName == ControllerName).FirstOrDefault().ControllerTypeInfo.CustomAttributes.Any() ? TotalList.Where(w => w.ControllerName == ControllerName).FirstOrDefault().ControllerTypeInfo.GetCustomAttribute<DescriptionAttribute>().Description : "Without Name",
                ControllerName = ControllerName.Replace("Controller", ""),
                Permissions = TotalList.Where(w => w.ControllerName == ControllerName).Select(Action => new PermissionDto
                {
                    Action = Action.ActionName,
                    ActionType = Action.MethodInfo.GetCustomAttribute<PermissionAttribute>().ActionType,
                    Controller = ControllerName,
                    Name = Action.MethodInfo.GetCustomAttribute<PermissionAttribute>().Name
                }).ToList()
            }).ToList();

            return Ok(await _permissionService.UpdateList(PermissionGroups));
        }
    }
}